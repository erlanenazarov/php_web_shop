//var $ = jQuery;
// Abstract rules
$(document).ready(function () {
    var productImageWrapper = $('.product-image-wrapper');

    productImageWrapper.each(function (i, obj) {
        var $this = $(obj);
        var image = $this.find('img');
        var imageWidth = $(image).width();
        var imageHeight = $(image).height();

        if (imageWidth > imageHeight) {
            $(image).css('max-width', '100%');
        } else {
            $(image).css('max-height', '100%');
        }
    });


    var increaseButton = $('#increase-count');
    var decreaseButton = $('#decrease-count');
    var productCount = $('#product-count');

    increaseButton.on('click', function (e) {
        e.preventDefault();
        var count = productCount.val();
        count++;
        productCount.val(count);
    });

    decreaseButton.on('click', function (e) {
        e.preventDefault();
        var count = productCount.val();
        if (count > 1) count--;
        productCount.val(count);
    });


    var addToBasketButtons = $('.product-add-to-basket-button');
    var addToBasketForm = $('#add-to-basket-form');
    var closeAddToBasketModal = $('#close-add-to-basket-modal');
    var addToBasketModal = $('#add-to-basket-modal');

    var basketAbstractCount = $('#abstract-basket-count');
    var indexPageBasketCount = $('#index-page-basket-count');

    addToBasketButtons.each(function () {
        $(this).on('click', function (e) {
            addToBasketForm.attr('action', $(this).attr('href'));
        });
    });

    closeAddToBasketModal.on('click', function () {
        addToBasketForm.attr('action', '#');
    });

    addToBasketForm.on('submit', function (e) {
        e.preventDefault();

        var url = $(this).attr('action');

        if (url !== '#') {
            $.ajax({
                url: url,
                method: 'POST',
                dataType: 'JSON',
                data: {
                    count: productCount.val()
                },
                success: function (response) {
                    addToBasketModal.foundation('reveal', 'close');
                    $.alert({
                        title: response.success ? 'Успех!' : 'Ошибка!',
                        content: response.message,
                        type: response.success ? 'green' : 'red',
                        typeAnimated: true,
                        buttons: {
                            close: {
                                text: 'Закрыть'
                            }
                        }
                    });

                    if (response.success && response.mode === 'add') {
                        var count = parseInt(basketAbstractCount.html(), 10);
                        count++;
                        basketAbstractCount.html(count);
                        indexPageBasketCount.html(count);
                    }
                },
                error: function () {
                    addToBasketModal.foundation('reveal', 'close');
                    $.alert({
                        title: 'Ошибка!',
                        content: 'Сервер не отвечает! Попробуйте позже!',
                        type: 'red',
                        typeAnimated: true,
                        buttons: {
                            close: {
                                text: 'Закрыть'
                            }
                        }
                    });
                }
            });
        }
    });
});



// Basket page
$(document).ready(function () {
    var productCountArea = $('.product-count-area');

    var removeFromBasket = $('.remove-from-basket');

    removeFromBasket.each(function (i, obj) {
        var $this = $(obj);

        $this.on('click', function (e) {
            e.preventDefault();
            var url = $this.attr('href');

            $.ajax({
                url: url,
                method: 'POST',
                dataType: 'JSON',
                success: function (response) {
                    if (response.success) {
                        var holder = $this.parent().parent();
                        holder.fadeOut('slow', function () {
                            holder.remove();
                            var basketAbstractCount = $('#abstract-basket-count');
                            var count = parseInt(basketAbstractCount.html(), 10);
                            if (count > 0) count--;
                            basketAbstractCount.html(count);

                            if (count === 0) {
                                var table = $('#basket-table');
                                table.append(
                                    '<tr><td colspan="6">Ваша корзина пуста...</td></tr>'
                                );
                            }

                            updateBasketTotal();
                        });
                    } else {
                        $.alert({
                            title: 'Ошибка!',
                            content: response.message,
                            type: 'red',
                            typeAnimated: true,
                            buttons: {
                                close: {
                                    text: 'Закрыть'
                                }
                            }
                        });
                    }
                },
                error: function () {
                    $.alert({
                        title: 'Ошибка!',
                        content: 'Сервер не отвечает! Попробуйте позже!',
                        type: 'red',
                        typeAnimated: true,
                        buttons: {
                            close: {
                                text: 'Закрыть'
                            }
                        }
                    });
                }
            });
        });
    });

    productCountArea.each(function (i, obj) {
        var $this = $(obj);
        var input = $this.find('input');
        var increase = $this.find('button.increase');
        var decrease = $this.find('button.decrease');

        increase.on('click', function () {
            var count = input.val();
            count++;
            input.val(count);
            input.trigger('change');
        });

        decrease.on('click', function () {
            var count = input.val();
            if (count > 1) count--;
            input.val(count);
            input.trigger('change');
        });

        input.on('change', function (e) {
            var url = $this.attr('data-url');
            var count = $(this).val();

            $.ajax({
                url: url,
                method: 'POST',
                dataType: 'JSON',
                data: {
                    count: count
                },
                success: function (response) {
                    if (response.success) {
                        updateBasketTotal();
                        var subTotal = $this.parent().find('td.sub-total-area');
                        subTotal.html(response.sub_total + ' ₽');
                    } else {
                        $.alert({
                            title: 'Ошибка!',
                            content: response.message,
                            type: 'red',
                            typeAnimated: true,
                            buttons: {
                                close: {
                                    text: 'Закрыть'
                                }
                            }
                        });
                    }
                },
                error: function () {
                    $.alert({
                        title: 'Ошибка!',
                        content: 'Сервер не отвечает! Попробуйте позже!',
                        type: 'red',
                        typeAnimated: true,
                        buttons: {
                            close: {
                                text: 'Закрыть'
                            }
                        }
                    });
                }
            });
        });
    });

    function updateBasketTotal() {
        var url = '/cart/total-price/get/';
        var basketTotalPrice = $('#basket-total-price');
        $.ajax({
            url: url,
            method: 'POST',
            dataType: 'JSON',
            success: function (response) {
                if (response.success) {
                    basketTotalPrice.html(response.total + ' ₽');
                } else {
                    $.alert({
                        title: 'Ошибка!',
                        content: response.message,
                        type: 'red',
                        typeAnimated: true,
                        buttons: {
                            close: {
                                text: 'Закрыть'
                            }
                        }
                    });
                }
            },
            error: function () {
                $.alert({
                    title: 'Ошибка!',
                    content: 'Сервер не отвечает! Попробуйте позже!',
                    type: 'red',
                    typeAnimated: true,
                    buttons: {
                        close: {
                            text: 'Закрыть'
                        }
                    }
                });
            }
        });
    }
});