<div class="row">
    <div class="large-12 columns">

        <?php include ('partial/top.bar.html.php'); ?>

        <div class="container" style="margin-top: 1.25rem;">
            <div class="row">
                <div class="large-12 columns">
                    <ul class="breadcrumbs" style="margin-bottom: 0">
                        <li><a href="/">Главная</a></li>
                        <li class="current">Корзина</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="row">
            <hr>
            <div class="large-12 columns">
                <table style="width: 100%">
                    <thead>
                    <tr>
                        <th>Товар</th>
                        <th>Количество</th>
                        <th>Цена</th>
                        <th>Итого</th>
                        <th width="80"></th>
                    </tr>
                    </thead>
                    <tbody id="basket-table">
                    <?php if($data['basket']['items'] !== null): ?>
                        <?php foreach ($data['basket']['items'] as $item): ?>
                            <tr>
                                <td>
                                    <a href="/products/<?=$item['product']->slug?>/"><?=$item['product']->title?></a>
                                </td>
                                <td class="product-count-area" data-url="/cart/item/<?=$item['id']?>/update/">
                                    <div>
                                        <button class="decrease"><i class="fa fa-minus"></i></button>
                                    </div>
                                    <div>
                                        <input type="number" placeholder="Количество" value="<?=$item['quantity']?>" />
                                    </div>
                                    <div>
                                        <button class="increase"><i class="fa fa-plus"></i></button>
                                    </div>
                                </td>
                                <td><?=$item['product']->price?> ₽</td>
                                <td class="sub-total-area"><?=$item['sub_total']?> ₽</td>
                                <td>
                                    <a href="/cart/<?=$item['id']?>/delete/" data-tooltip aria-haspopup="true" title="Убрать из корзины" class="remove-from-basket"><i class="fa fa-times"></i></a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php else: ?>
                    <tr>
                        <td colspan="5">Ваша корзина пуста...</td>
                    </tr>
                    <?php endif; ?>
                    </tbody>
                </table>
                <div class="row">
                    <div class="large-4 large-offset-8 columns">
                        <div class="panel">
                            <div class="row">
                                <div class="medium-6 columns text-right">
                                    <h4 class="subheader">Итого:</h4>
                                </div>
                                <div class="medium-6 columns text-right">
                                    <h4 class="subheader" id="basket-total-price"><?=$data['basket']['total']?> ₽</h4>
                                </div>
                                <div class="medium-12 columns">
                                    <button class="button" style="width: 100%;" data-reveal-id="checkout-modal">Оформить заказ</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div id="checkout-modal" class="reveal-modal small" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
    <h2 id="modalTitle">Заполните информацию</h2>
    <form action="" method="POST">
        <div class="row">
            <div class="large-12 columns">
                <div class="row">
                    <div class="small-3 columns">
                        <label for="address" class="right inline">Адрес</label>
                    </div>
                    <div class="small-9 columns">
                        <input type="text" id="address" placeholder="..." name="address"<?php if (Security::getInstance()->isAuth()): ?> value="<?=Security::getInstance()->getUser()->address?>"<?php endif; ?>>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="large-12 columns">
                <div class="row">
                    <div class="small-3 columns">
                        <label for="phone" class="right inline">Телефон</label>
                    </div>
                    <div class="small-9 columns">
                        <input type="text" id="phone" placeholder="..." name="phone"<?php if(Security::getInstance()->isAuth()): ?> value="<?=Security::getInstance()->getUser()->phone?>"<?php endif; ?>>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="large-12 columns">
                <div class="small-9 small-offset-3 columns">
                    <button class="btn btn-blue">Заказать</button>
                </div>
            </div>
        </div>
    </form>
    <a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>