<div class="row">
    <div class="large-12 columns">

        <?php include ('partial/top.bar.html.php'); ?>


        <div class="container" style="margin-top: 1.25rem;">
            <div class="row">
                <div class="large-12 columns">
                    <ul class="breadcrumbs" style="margin-bottom: 0">
                        <li><a href="/">Главная</a></li>
                        <li class="current">Профиль</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="row">
            <hr />
            <div class="large-12 columns">
                <h4>Информация</h4>
            </div>
            <form class="container">
                <div class="row">
                    <div class="large-6 columns">
                        <div class="row">
                            <div class="small-3 columns">
                                <label for="right-label" class="right inline">Имя</label>
                            </div>
                            <div class="small-9 columns">
                                <input type="text" id="right-label" placeholder="..." name="nickname" value="<?=$data['user']->nickName?>">
                            </div>
                        </div>
                    </div>
                    <div class="large-6 columns">
                        <div class="row">
                            <div class="small-3 columns">
                                <label for="email" class="right inline">E-Mail</label>
                            </div>
                            <div class="small-9 columns">
                                <input type="text" id="email" placeholder="..." value="<?=$data['user']->email?>" disabled class="disabled">
                            </div>
                        </div>
                    </div>
                    <div class="large-6 columns">
                        <div class="row">
                            <div class="small-3 columns">
                                <label for="phone" class="right inline">Телефон</label>
                            </div>
                            <div class="small-9 columns">
                                <input type="text" id="phone" placeholder="..." value="<?=$data['user']->phone?>" name="phone">
                            </div>
                        </div>
                    </div>
                    <div class="large-6 columns">
                        <div class="row">
                            <div class="small-3 columns">
                                <label for="address" class="right inline">Адрес</label>
                            </div>
                            <div class="small-9 columns">
                                <input type="text" id="address" placeholder="..." value="<?=$data['user']->address?>" name="address">
                            </div>
                        </div>
                    </div>
                    <div class="large-12 columns">
                        <div class="row">
                            <div class="large-6 large-offset-1 columns">
                                <button type="submit" class="btn btn-blue">Сохранить</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="row">
            <hr />
            <div class="large-12 columns">
                <h4>История заказов</h4>

            </div>
        </div>
    </div>
</div>