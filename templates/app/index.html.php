<div class="row">
    <div class="large-12 columns">

        <?php include ('partial/top.bar.html.php'); ?>

        <div class="row">

            <div class="large-4 small-12 columns">
                <img src="/public/images/G-PwxA4ZIi0.jpg">
                <div class="hide-for-small panel">
                    <h3>Цифровой бутик</h3>
                    <h5 class="subheader">У нас вы сэкономите до 70%</h5>
                </div>
                <a href="/cart/">
                    <div class="panel callout radius">
                        <h6><kbd id="index-page-basket-count"><?=$data['basket_count']?></kbd>&nbsp; Товаров в корзине</h6>
                    </div>
                </a>
            </div>

            <div class="large-8 columns">
                <div class="products-container">
                    <h3>Последние добавленные товары</h3>
                    <div class="row">
                        <?php include ('partial/misc/product.list.html.php'); ?>
                    </div>
                </div>
            </div>
        </div>