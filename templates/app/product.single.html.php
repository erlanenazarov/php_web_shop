<div class="row">
    <div class="large-12 columns">

        <?php include ('partial/top.bar.html.php'); ?>


        <div class="container" style="margin-top: 1.25rem;">
            <div class="row">
                <div class="large-12 columns">
                    <ul class="breadcrumbs" style="margin-bottom: 0">
                        <li><a href="/">Главная</a></li>
                        <?php if($data['product']->category->parent_category !== null): ?>
                            <li><a href="/categories/<?=$data['product']->category->parent_category->slug?>/"><?=$data['product']->category->parent_category->title?></a></li>
                            <li><a href="/categories/<?=$data['product']->category->parent_category->slug?>/<?=$data['product']->category->slug?>/"><?=$data['product']->category->title?></a></li>
                        <?php else: ?>
                            <li><a href="/categories/<?=$data['product']->category->slug?>/"><?=$data['product']->category->title?></a></li>
                        <?php endif; ?>
                        <li class="current"><?=$data['product']->title?></li>
                    </ul>
                </div>
            </div>
        </div>
        
        <div class="row">
            <hr>
            <div class="large-5 columns">
                <img src="<?=$data['product']->image['url']?>">
            </div>
            <div class="large-7 columns">
                <h4><?=$data['product']->title?></h4>
                <p><?=$data['product']->shortDescription?></p>
                <div class="panel">
                    <h5>Цена: <?=$data['product']->price?> ₽</h5>
                    <h6 class="subheader">Спешите купить у нас по самой низкой цене!</h6>
                    <a href="/cart/product/<?=$data['product']->slug?>/add/" class="small button product-add-to-basket-button" data-reveal-id="add-to-basket-modal"><i class="fa fa-shopping-basket"></i> Добавить в корзину</a>
                    <a href="/favorite/product/<?= $data['product']->slug ?>/add/" class="small button"><i class="fa fa-heart"></i> Добавить в избранное </a>
                </div>
            </div>
        </div>
        <div class="row">
            <hr />
            <div class="large-12 columns">
                <h4>Описание</h4>
                <p><?=$data['product']->fullDescription?></p>
            </div>
        </div>
<!--        <div class="row">-->
<!--            <hr>-->
<!--            <div class="large-12 columns">-->
<!--                <h4>Так же вам может понравится</h4>-->
<!--                <img src="https://placehold.it/200x150&text=[img]">-->
<!--                <img src="https://placehold.it/200x150&text=[img]">-->
<!--                <img src="https://placehold.it/200x150&text=[img]">-->
<!--                <img src="https://placehold.it/200x150&text=[img]">-->
<!--            </div>-->
<!--        </div>-->
    </div>
</div>