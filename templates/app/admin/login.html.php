<div class="container">
    <div class="row">
        <div class="large-4 large-offset-4">
            <form method="POST">
                <div class="form-icons">
                    <h4>Авторизация</h4>

                    <?php if(isset($data['error'])): ?>
                    <div class="alert warning"><?=$data['error']?></div>
                    <?php endif; ?>

                    <div class="input-group">
                        <input class="input-group-field" type="text" placeholder="Email" name="email">
                    </div>

                    <div class="input-group">
                        <input class="input-group-field" type="password" placeholder="Пароль" name="password">
                    </div>
                </div>

                <button class="button expanded">Войти</button>
            </form>
        </div>
    </div>
</div>