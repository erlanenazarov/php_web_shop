<?php include (PROJECT_PATH.'/templates/app/partial/admin/top.bar.php'); ?>



<div class="row">

    <div class="large-9 push-3 columns">
        <h3>Товары <small>создать - читать - редактировать - удалить</small></h3>

        <form method="POST" enctype="multipart/form-data">
            <fieldset>
                <?php if (isset($data['result'])): ?>
                    <div class="alert-box<?php if ($data['result']['success']):?> success<?php else: ?> alert<?php endif; ?>">
                        <?=$data['result']['message']?>
                    </div>
                <?php endif; ?>
                <div>
                    <label for="id_title">Название</label>
                    <input type="text" name="title" placeholder="Название" id="id_title" value="<?=$data['product']->title?>" required />
                </div>
                <div>
                    <label for="id_slug">Slug</label>
                    <input type="text" name="slug" placeholder="Slug" id="id_slug" value="<?=$data['product']->slug?>" required />
                </div>
                <div>
                    <img src="<?= $data['product']->image['url'] ?>" alt="<?= $data['product']->title ?>" style="max-width: 200px;">
                    <label for="id_image">Картинка</label>
                    <input type="file" id="id_image" name="image">
                </div>
                <div>
                    <label for="id_short_description">Короткое описание</label>
                    <textarea name="short_description" id="id_short_description" cols="30" rows="10" required><?=$data['product']->shortDescription?></textarea>
                </div>
                <div>
                    <label for="id_full_description">Полное описание</label>
                    <textarea name="full_description" id="id_full_description" cols="30" rows="10" required><?=$data['product']->fullDescription?></textarea>
                </div>
                <div>
                    <label for="id_rating">Рейтинг</label>
                    <input type="number" name="rating" id="id_rating" value="<?=$data['product']->rating?>" />
                </div>
                <div>
                    <label for="id_price">Цена</label>
                    <input type="number" name="price" id="id_price" value="<?=$data['product']->price?>" />
                </div>
                <div>
                    <label for="id_category">Категория</label>

                    <select id="id_category" name="category_id" required>
                        <option value="">Выберите из списка</option>
                        <?php for ($i=0; $i < sizeof($data['categories']); ++$i): ?>
                            <option value="<?=$data['categories'][$i]->id?>"<?php if ($data['product']->category->id == $data['categories'][$i]->id):?> selected<?php endif; ?>><?=$data['categories'][$i]->title?></option>
                        <?php endfor; ?>
                    </select>
                </div>
                <div>
                    <label for="id_is_active">Активна?</label>
                    <input type="checkbox" id="id_is_active" name="is_active"<?php if($data['product']->is_active): ?> checked<?php endif; ?> />
                </div>
            </fieldset>

            <fieldset>
                <button type="submit">Сохранить</button>
            </fieldset>
        </form>
    </div>

    <div class="large-3 pull-9 columns">
        <ul class="side-nav">
            <li><a href="/admin/products/">Все товары</a></li>
        </ul>
        <p><img src="https://placehold.it/320x240&text=Ad" /></p>
    </div>
</div>
