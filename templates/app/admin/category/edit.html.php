<?php include (PROJECT_PATH.'/templates/app/partial/admin/top.bar.php'); ?>



<div class="row">

    <div class="large-9 push-3 columns">
        <h3>Категории <small>создать - читать - редактировать - удалить</small></h3>

        <form method="POST">
            <fieldset>
                <?php if (isset($data['result'])): ?>
                    <div data-alert class="alert-box<?php if ($data['result']['success'] === true) echo (' success'); else echo(' danger'); ?> radius">
                        <?=$data['result']['message']?>
                    </div>
                <?php endif; ?>
                <div>
                    <label for="id_title">Название</label>
                    <input type="text" name="title" placeholder="Название" id="id_title" value="<?=$data['category']->title?>" />
                </div>
                <div>
                    <label for="id_slug">Slug</label>
                    <input type="text" name="slug" placeholder="Slug" id="id_slug" value="<?=$data['category']->slug?>" />
                </div>
                <div>
                    <label for="id_parent_category">Родительская категория</label>

                    <select id="id_parent_category" name="parent_category">
                        <option value="">Выберите из списка</option>
                        <?php for ($i=0; $i < sizeof($data['categories']); ++$i): ?>
                            <?php if ($data['categories'][$i]->id !== $data['category']->id): ?>
                            <option value="<?=$data['categories'][$i]->id?>"<?php if ($data['categories'][$i]->parent_category !== null && (int)$data['categories'][$i]->id === $data['category']->parent_category): ?> selected<?php endif; ?>><?=$data['categories'][$i]->title?></option>
                            <?php endif; ?>
                        <?php endfor; ?>
                    </select>
                </div>
            </fieldset>

            <fieldset>
                <button type="submit">Сохранить</button>
            </fieldset>
        </form>
    </div>


    <div class="large-3 pull-9 columns">
        <ul class="side-nav">
            <li><a href="#">Все категории</a></li>
        </ul>
        <p><img src="https://placehold.it/320x240&text=Ad" /></p>
    </div>
</div>
