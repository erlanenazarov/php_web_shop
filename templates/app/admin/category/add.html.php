<?php include (PROJECT_PATH.'/templates/app/partial/admin/top.bar.php'); ?>



<div class="row">

    <div class="large-9 push-3 columns">
        <h3>Категории <small>создать - читать - редактировать - удалить</small></h3>

        <form method="POST">
            <fieldset>
                <?php if (isset($data['result'])): ?>
                    <div class="alert-box<?php if ($data['result']['success']): ?> success<?php else: ?> alert<?php endif; ?>">
                        <?=$data['result']['message']?>
                    </div>
                <?php endif; ?>
                <div>
                    <label for="id_title">Название</label>
                    <input type="text" name="title" placeholder="Название" id="id_title" value="<?=$_POST['title']?>" />
                </div>
                <div>
                    <label for="id_slug">Slug</label>
                    <input type="text" name="slug" placeholder="Slug" id="id_slug" value="<?=$_POST['slug']?>" />
                </div>
                <div>
                    <label for="id_parent_category">Родительская категория</label>

                    <select id="id_parent_category" name="parent_category">
                        <option value="">Выберите из списка</option>
                        <?php for ($i=0; $i < sizeof($data['categories']); ++$i): ?>
                            <option value="<?=$data['categories'][$i]->id?>"><?=$data['categories'][$i]->title?></option>
                        <?php endfor; ?>
                    </select>
                </div>
            </fieldset>

            <fieldset>
                <button type="submit">Сохранить</button>
            </fieldset>
        </form>
    </div>


    <div class="large-3 pull-9 columns">
        <ul class="side-nav">
            <li><a href="#">Все категории</a></li>
        </ul>
        <p><img src="https://placehold.it/320x240&text=Ad" /></p>
    </div>
</div>
