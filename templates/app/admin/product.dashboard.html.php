<?php //var_dump($data); ?>

<?php include (PROJECT_PATH.'/templates/app/partial/admin/top.bar.php'); ?>

<div class="row">

    <div class="large-9 push-3 columns">
        <h3>Пользователи <small>создать - читать - редактировать - удалить</small></h3>
        <?php if (isset($_REQUEST['error']) || isset($_REQUEST['success'])): ?>
            <div class="alert-box <?php if(isset($_REQUEST['error']) && !isset($_REQUEST['success'])) { echo('alert'); } else { echo('success'); } ?>">
                <?php if (isset($_REQUEST['error']) && !isset($_REQUEST['success'])): ?>
                    Произошла ошибка при проведении операции
                <?php else: ?>
                    Операция прошла успешно!
                <?php endif; ?>
            </div>
        <?php endif; ?>
        <div class="filter-block">
            <div>
                <form style="min-width: 300px">
                    <div style="position: relative;">
                        <input placeholder="Поиск..." name="search_word" style="display: inline-block;" value="<?=$data['search_word']?>" />
                        <span class="button" style="padding: 9px 12px; position: absolute; right: 0; top: 0;">
                            <i class="fa fa-search"></i>
                        </span>
                    </div>
                </form>
            </div>
            <div>
                <a href="/admin/products/create/" class="button primary">Создать</a>
            </div>
        </div>
        <table style="max-width: 100%;">
            <thead>
            <tr>
                <th width="50">id</th>
                <th>Название</th>
                <th width="150">slug</th>
                <th width="150">Картинка</th>
                <th width="150">Рейтинг</th>
                <th width="150">Категория</th>
                <th width="80">Активна?</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($data['Products'] as $product): ?>
                <tr>
                    <td>#<?=$product->id?></td>
                    <td><a href="/admin/products/<?=$product->id?>/change/"><?=$product->title?></a></td>
                    <td><?=$product->slug?></td>
                    <td><a href="<?= $product->image['url'] ?>" target="_blank"><?=$product->image['name']?></a></td>
                    <td><?=$product->rating?></td>
                    <td>
                        <?php if ($product->category !== null): ?>
                            <?=$product->category->title?>
                        <?php else: ?>
                            null
                        <?php endif; ?>
                    </td>
                    <td class="boolean-field">
                        <?php if($product->is_active):?>
                            <i class="fa fa-check"></i>
                        <?php else: ?>
                            <i class="fa fa-times"></i>
                        <?php endif; ?>
                    </td>
                    <td>
                        <a href="/admin/products/<?=$product->id?>/delete/" class="button alert">Удалить</a>
                    </td>
                </tr>
            <?php endforeach; ?>
            <?php if (sizeof($data['Products']) <= 0): ?>
                <tr>
                    <td colspan="8">Товаров нет в базе...</td>
                </tr>
            <?php endif; ?>
            </tbody>
        </table>
    </div>


    <div class="large-3 pull-9 columns">
        <ul class="side-nav">
            <li><a href="/admin/products/">Все товары</a></li>
        </ul>
        <p><img src="https://placehold.it/320x240&text=Ad" /></p>
    </div>
</div>