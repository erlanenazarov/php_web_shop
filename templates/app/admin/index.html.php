<?php //var_dump($data); ?>

<?php include (PROJECT_PATH.'/templates/app/partial/admin/top.bar.php'); ?>


<div class="row">

    <div class="large-9 push-3 columns">
        <h3>Категории <small>создать - читать - редактировать - удалить</small></h3>
        <div class="filter-block">
            <div>
                <form style="min-width: 300px">
                    <div style="position: relative;">
                        <input placeholder="Поиск..." name="search_word" style="display: inline-block;" value="<?=$data['search_word']?>" />
                        <span class="button" style="padding: 9px 12px; position: absolute; right: 0; top: 0;">
                            <i class="fa fa-search"></i>
                        </span>
                    </div>
                </form>
            </div>
            <div>
                <a href="/admin/category/create/" class="button primary">Создать</a>
            </div>
        </div>
        <table style="width: 100%;">
            <thead>
            <tr>
                <th width="50">id</th>
                <th>Название</th>
                <th width="150">Slug</th>
                <th width="150">Родительская категория</th>
                <th width="80"></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($data['categories'] as $cat): ?>
                <tr>
                    <td>#<?=$cat->id?></td>
                    <td><a href="/admin/category/<?=$cat->id?>/change/"><?=$cat->title?></a></td>
                    <td><?=$cat->slug?></td>
                    <td>
                        <?=$cat->parent_category?>
                    </td>
                    <td>
                        <a href="/admin/category/<?=$cat->id?>/delete/" class="button alert">Удалить</a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>


    <div class="large-3 pull-9 columns">
        <ul class="side-nav">
            <li><a href="#">Все категории</a></li>
        </ul>
        <p><img src="https://placehold.it/320x240&text=Ad" /></p>
    </div>
</div>