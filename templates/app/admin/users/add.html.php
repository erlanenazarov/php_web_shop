<?php include (PROJECT_PATH.'/templates/app/partial/admin/top.bar.php'); ?>



<div class="row">

    <div class="large-9 push-3 columns">
        <h3>Пользователи <small>создать - читать - редактировать - удалить</small></h3>

        <form method="POST">
            <fieldset>
                <?php if (isset($data['result'])): ?>
                    <div class="alert-box alert">
                        <?=$data['result']['message']?>
                    </div>
                <?php endif; ?>
                <div>
                    <label for="id_nickname">Имя пользователя</label>
                    <input type="text" name="nickname" placeholder="Имя пользователя" id="id_nickname" value="<?=$_POST['nickname']?>" autocomplete="new-nickname" />
                </div>
                <div>
                    <label for="id_email">E-Mail</label>
                    <input type="text" name="email" placeholder="E-Mail" id="id_email" value="<?=$_POST['email']?>" />
                </div>
                <div>
                    <label for="id_password">Пароль</label>
                    <input type="password" name="password" placeholder="Пароль" id="id_password" value="<?=$_POST['password']?>" autocomplete="new-password" />
                </div>
                <div>
                    <label for="id_is_superuser">Суперпользователь?</label>
                    <input type="checkbox" name="is_superuser" id="id_is_superuser"<?php if ($_POST['is_superuser'] == 'on'): ?> checked<?php endif; ?> />
                </div>
            </fieldset>

            <fieldset>
                <button type="submit">Сохранить</button>
            </fieldset>
        </form>
    </div>


    <div class="large-3 pull-9 columns">
        <ul class="side-nav">
            <li><a href="/admin/users/">Все пользователи</a></li>
        </ul>
        <p><img src="https://placehold.it/320x240&text=Ad" /></p>
    </div>
</div>
