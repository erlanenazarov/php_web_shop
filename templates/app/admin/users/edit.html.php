<?php include (PROJECT_PATH.'/templates/app/partial/admin/top.bar.php'); ?>

<div class="row">

    <div class="large-9 push-3 columns">
        <h3>Пользователи <small>создать - читать - редактировать - удалить</small></h3>

        <form method="POST">
            <fieldset>
                <?php if (isset($data['result'])): ?>
                    <div class="alert-box<?php if (isset($data['result']) && $data['result']['success']) { echo(' success'); } else if (isset($data['result']) && !$data['result']['success']) { echo(' alert'); }?>">
                        <?=$data['result']['message']?>
                    </div>
                <?php endif; ?>
                <?php if (isset($_REQUEST['success'])): ?>
                    <div class="alert-box success">
                        Пароль успешно изменен!
                    </div>
                <?php endif; ?>
                <?php if (isset($_REQUEST['error'])): ?>
                    <div class="alert-box alert">
                        Произошла ошибка при изменении пароля!
                    </div>
                <?php endif; ?>
                <div>
                    <label for="id_nickname">Имя пользователя</label>
                    <input type="text" name="nickname" placeholder="Имя пользователя" id="id_nickname" value="<?=$data['user']->nickName?>" autocomplete="new-nickname" />
                </div>
                <div>
                    <label for="id_email">E-Mail</label>
                    <input type="email" name="email" placeholder="E-Mail" id="id_email" value="<?=$data['user']->email?>" />
                </div>
                <div>
                    <label for="id_password">Пароль</label>
                    <input type="password" placeholder="Пароль" id="id_password" value="<?=md5($data['user']->password)?>" autocomplete="new-password" disabled class="disabled" />
                    <p class="help-text">Пароль храниться в базе в зашифрованном виде! Чтобы изменить пароль воспользуйтесь
                        <a href="/admin/users/<?=$data['user']->id?>/change/password/">этой формой</a></p>
                </div>
                <div>
                    <label for="id_is_superuser">Суперпользователь?</label>
                    <input type="checkbox" name="is_superuser" id="id_is_superuser"<?php if ($data['user']->is_superuser): ?> checked value="on"<?php endif; ?> />
                </div>
            </fieldset>

            <fieldset>
                <button type="submit">Сохранить</button>
            </fieldset>
        </form>
    </div>


    <div class="large-3 pull-9 columns">
        <ul class="side-nav">
            <li><a href="/admin/users/">Все пользователи</a></li>
        </ul>
        <p><img src="https://placehold.it/320x240&text=Ad" /></p>
    </div>
</div>
