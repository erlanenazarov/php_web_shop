<?php include (PROJECT_PATH.'/templates/app/partial/admin/top.bar.php'); ?>

<div class="row">

    <div class="large-9 push-3 columns">
        <h3>Пользователи <small>создать - читать - редактировать - удалить</small></h3>

        <form method="POST">
            <fieldset>
                <?php if (isset($data['result'])): ?>
                    <div class="alert-box<?php if (isset($data['result']) && $data['result']['success']) { echo(' success'); } else if (isset($data['result']) && !$data['result']['success']) { echo(' alert'); }?>">
                        <?=$data['result']['message']?>
                    </div>
                <?php endif; ?>
                <div>
                    <label for="id_password1">Пароль</label>
                    <input type="password" name="password1" placeholder="Новый пароль" id="id_password1" autocomplete="new-password" />
                </div>
                <div>
                    <label for="id_password2">Повторите пароль</label>
                    <input type="password" name="password2" placeholder="Повторите пароль" id="id_password2" autocomplete="re-new-password" />
                </div>
            </fieldset>

            <fieldset>
                <button type="submit">Сохранить</button>
                <a href="/admin/users/<?= $data['user']->id ?>/change/" class="button">Назад</a>
            </fieldset>
        </form>
    </div>


    <div class="large-3 pull-9 columns">
        <ul class="side-nav">
            <li><a href="/admin/users/">Все пользователи</a></li>
        </ul>
        <p><img src="https://placehold.it/320x240&text=Ad" /></p>
    </div>
</div>
