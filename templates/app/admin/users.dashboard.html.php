<?php //var_dump($data); ?>

<?php include (PROJECT_PATH.'/templates/app/partial/admin/top.bar.php'); ?>


<div class="row">

    <div class="large-9 push-3 columns">
        <h3>Пользователи <small>создать - читать - редактировать - удалить</small></h3>
        <?php if (isset($_REQUEST['error']) || isset($_REQUEST['success'])): ?>
            <div class="alert-box <?php if(isset($_REQUEST['error']) && !isset($_REQUEST['success'])) { echo('alert'); } else { echo('success'); } ?>">
                <?php if (isset($_REQUEST['error']) && !isset($_REQUEST['success'])): ?>
                    Произошла ошибка при проведении операции
                <?php else: ?>
                    Операция прошла успешно!
                <?php endif; ?>
            </div>
        <?php endif; ?>
        <div class="filter-block">
            <div>
                <form style="min-width: 300px">
                    <div style="position: relative;">
                        <input placeholder="Поиск..." name="search_word" style="display: inline-block;" value="<?=$data['search_word']?>" />
                        <span class="button" style="padding: 9px 12px; position: absolute; right: 0; top: 0;">
                            <i class="fa fa-search"></i>
                        </span>
                    </div>
                </form>
            </div>
            <div>
                <a href="/admin/users/create/" class="button primary">Создать</a>
            </div>
        </div>
        <table style="width: 100%;">
            <thead>
            <tr>
                <th width="50">id</th>
                <th>Имя пользователя</th>
                <th width="150">email</th>
                <th width="150">Корзина</th>
                <th width="150">Роль</th>
                <th width="150">Админ</th>
                <th width="80"></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($data['users'] as $user): ?>
                <tr>
                    <td>#<?=$user->id?></td>
                    <td><?=$user->nickName?></td>
                    <td><a href="/admin/users/<?=$user->id?>/change/"><?=$user->email?></a></td>
                    <td><?=$user->cart_id?></td>
                    <td><?=$user->role?></td>
                    <td class="boolean-field">
                        <?php if($user->is_superuser):?>
                            <i class="fa fa-check"></i>
                        <?php else: ?>
                            <i class="fa fa-times"></i>
                        <?php endif; ?>
                    </td>
                    <td>
                        <a href="/admin/users/<?=$user->id?>/delete/" class="button alert">Удалить</a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>


    <div class="large-3 pull-9 columns">
        <ul class="side-nav">
            <li><a href="/admin/users/">Все пользователи</a></li>
        </ul>
        <p><img src="https://placehold.it/320x240&text=Ad" /></p>
    </div>
</div>