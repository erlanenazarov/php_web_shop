<div class="container">
    <div class="row">
        <div class="large-4 large-offset-4">
            <form method="POST">
                <div class="form-icons">
                    <h4>Зарегистрироваться</h4>

                    <?php if(isset($data['error'])): ?>
                        <div class="alert warning"><?=$data['error']?></div>
                    <?php endif; ?>

                    <div class="input-group">
                        <input class="input-group-field" type="text" placeholder="Полное имя" name="nickname">
                    </div>

                    <div class="input-group">
                        <input class="input-group-field" type="text" placeholder="Email" name="email">
                    </div>

                    <div class="input-group">
                        <input class="input-group-field" type="text" placeholder="Пароль" name="password">
                    </div>

                    <div class="input-group">
                        <input class="input-group-field" type="text" placeholder="Повторите пароль" name="re_password">
                    </div>
                </div>

                <button type="submit" class="button expanded">Зарегистрировться</button>
            </form>
        </div>
    </div>
</div>