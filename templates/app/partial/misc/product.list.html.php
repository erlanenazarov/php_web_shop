<?php foreach ($data['products'] as $product): ?>
    <div class="large-4 small-6 columns end product">
        <div class="product-actions panel">
            <a href="/cart/product/<?= $product->slug ?>/add/" data-reveal-id="add-to-basket-modal" data-tooltip aria-haspopup="true" class="product-add-to-basket-button" title="Добавить в корзину"><i class="fa fa-shopping-basket"></i></a>
            <a href="/favorite/product/<?= $product->slug ?>/add/" data-tooltip aria-haspopup="true" class="has-tip" title="Добавить в избранное"><i class="fa fa-heart"></i></a>
        </div>
        <div class="product-image-wrapper">
            <img src="<?=$product->image['url']?>">
        </div>
        <div class="panel product-info">
            <h5><a href="/products/<?=$product->slug?>/"><?=$product->title?></a></h5>
            <!--<p>-->
            <!--=$product->shortDescription-->
            <!--</p>-->
            <div class="row">
                <div class="large-6 small-6 columns">
                    <h6 class="subheader"><?=$product->price?> ₽</h6>
                </div>
                <div class="large-6 small-6 columns text-right">
                    <h6 class="subheader"><i class="fa fa-bar-chart-o" data-tooltip aria-haspopup="true" title="Рейтинг товара"></i> <?=$product->rating?></h6>
                </div>
            </div>
            <p class="label info"><?=$product->category->title?></p>
        </div>
    </div>
<?php endforeach; ?>