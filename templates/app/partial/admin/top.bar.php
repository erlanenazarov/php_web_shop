<div class="contain-to-grid sticky">
    <nav class="top-bar" data-topbar role="navigation" data-options="sticky_on: large">
        <ul class="title-area">
            <li class="name">
                <h1><a href="/admin/">Администрация</a></h1>
            </li>
            <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
            <li class="toggle-topbar menu-icon"><a href="#"><span>Меню</span></a></li>
        </ul>

        <section class="top-bar-section">
            <!-- Right Nav Section -->
            <ul class="right">
                <li<?php if ($data['location'] === 'categories'): ?> class="active"<?php endif; ?>><a href="/admin/">Категории</a></li>
                <li<?php if ($data['location'] === 'Products'): ?> class="active"<?php endif; ?>><a href="/admin/products/">Товары</a></li>
                <li<?php if ($data['location'] === 'orders'): ?> class="active"<?php endif; ?>><a href="/admin/orders/">Заказы</a></li>
                <li<?php if ($data['location'] === 'users'): ?> class="active"<?php endif; ?>><a href="/admin/users/">Пользователи</a></li>
            </ul>
    </nav>
</div>