<footer class="row">
    <div class="large-12 columns">
        <hr>
        <div class="row">
            <div class="large-6 columns">
                <p>© 2018 Дмитрий Шамсулин.</p>
            </div>
            <div class="large-6 columns">
                <ul class="inline-list right">
                    <li>
                        <a href="/">Главная</a>
                    </li>
                    <li>
                        <a href="#">Категории</a>
                    </li>
                    <li>
                        <a href="#">Корзина</a>
                    </li>
                    <li>
                        <?php if(Security::getInstance()->isAuth()): ?>
                            <a href="/auth/logout/?next=<?=$_SERVER['REQUEST_URI']?>">Выход</a>
                        <?php else: ?>
                            <a href="/auth/login/">Вход</a>
                        <?php endif; ?>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>
</div>
</div>


<div id="add-to-basket-modal" class="reveal-modal medium" data-reveal aria-labelledby="addToBasket" aria-hidden="true" role="dialog">
    <h2 id="modalTitle" class="text-center" style="margin-bottom: 20px;">Добавить товар в корзину</h2>
    <form action="#" method="POST" id="add-to-basket-form">
        <div class="row">
            <div class="medium-5 columns text-right">
                <button id="decrease-count"><i class="fa fa-minus"></i></button>
            </div>
            <div class="medium-2 columns">
                <input type="number" placeholder="Количество" id="product-count" style="height: 3.2rem;" min="1" step="1" value="1">
            </div>
            <div class="medium-5 columns text-left">
                <button id="increase-count"><i class="fa fa-plus"></i></button>
            </div>
        </div>
        <div class="row">
            <div class="medium-12 columns text-center">
                <button type="submit">Добавить в корзину</button>
            </div>
        </div>
    </form>
    <a class="close-reveal-modal" aria-label="Close" id="close-add-to-basket-modal">&#215;</a>
</div>

<script src="/public/js/vendor/jquery.js"></script>
<script src="/public/js/vendor/jquery.confirm.js"></script>
<script src="/public/js/foundation.min.js"></script>
<script src="/public/js/app.js"></script>
<script>
    $(document).foundation();
</script>
</body>
</html>