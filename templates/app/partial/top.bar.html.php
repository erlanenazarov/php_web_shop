<div class="row">
    <div class="large-12 columns">
        <nav class="top-bar" data-topbar>
            <ul class="title-area">

                <li class="name">
                    <h1><a href="/">Цифровой маркет</a></h1>
                </li>
                <li class="toggle-topbar menu-icon">
                    <a href="#"><span>Меню</span></a>
                </li>
            </ul>
            <section class="top-bar-section">

                <ul class="right">
                    <li class="divider"></li>
                    <li class="has-dropdown">
                        <a href="#">Каталог</a>
                        <ul class="dropdown">
                            <?php foreach($data['categories'] as $category): ?>
                            <li><a href="/categories/<?=$category['slug']?>/"><?=$category['title']?></a></li>
                                <?php if (sizeof($category['sub_categories']) > 0): ?>
                                    <?php foreach ($category['sub_categories'] as $sub_cat): ?>
                                        <li class="sub-cat">
                                            <a class="" href="/categories/<?=$category['slug']?>/<?=$sub_cat['slug']?>/"><?=$sub_cat['title']?></a>
                                        </li>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </ul>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="/cart/">Корзина <kbd id="abstract-basket-count"><?=$data['basket_count']?></kbd></a>
                    </li>
                    <li class="divider"></li>
                    <li class="has-dropdown">
                        <?php if (Security::getInstance()->isAuth()): ?>
                        <a href="/auth/profile/">Добро пожаловать, <?=Security::getInstance()->getUser()->nickName?></a>
                        <?php else: ?>
                        <a href="/auth/login/?next=/auth/profile/">Профиль</a>
                        <?php endif; ?>
                        <ul class="dropdown">
                            <?php if (Security::getInstance()->isAuth()): ?>
                            <li>
                                <a href="#">Настройки</a>
                            </li>
                            <li>
                                <a href="#">Мои заказы</a>
                            </li>
                            <li>
                                <a href="#">Избранные</a>
                            </li>
                            <li>
                                <a href="/auth/logout/">Выйти</a>
                            </li>
                            <?php else: ?>
                            <li>
                                <a href="/auth/login/">Войти</a>
                            </li>
                            <li>
                                <a href="/auth/register/">Регистрация</a>
                            </li>
                            <?php endif; ?>
                        </ul>
                    </li>
                </ul>
            </section>
        </nav>
    </div>
</div>