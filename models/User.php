<?php
/**
 * Created by PhpStorm.
 * User: erlan
 * Date: 5/25/17
 * Time: 4:14 PM
 */

class User extends Model{
    public $id;
    public $nickName;
    public $email;
    public $password;
    public $role;
    public $is_superuser = false;
    public $is_anonymous = false;

    public $address;
    public $phone;

    function __construct($id, $nickName, $email, $password, $role, $is_superuser, $address, $phone) {
        $this->id = $id;
        $this->nickName = $nickName;
        $this->email = $email;
        $this->password = $password;
        $this->role = $role;
        $this->is_superuser = $is_superuser;
        $this->is_anonymous = false;
        $this->address = $address;
        $this->phone = $phone;

        return $this;
    }
}