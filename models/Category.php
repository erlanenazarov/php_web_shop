<?php
/**
 * Created by PhpStorm.
 * User: erlan
 * Date: 16.03.2018
 * Time: 16:01
 */

class Category extends Model
{
    /**
     * @var integer
     * Id of an object
     */
    public $id;

    /**
     * @var string
     * Title of an category
     */
    public $title;

    /**
     * @var string
     * slug need for URL
     */
    public $slug;

    /**
     * @var object || null
     * If current category is sub category
     */
    public $parent_category = null;


    /**
     * @var array
     * All sub categories
     */
    public $sub_categories = array();


    function __construct($id, $title, $slug, $parent_category)
    {
        $this->id = $id;
        $this->title = $title;
        $this->slug = $slug;
        if ($parent_category !== null)
            $this->parent_category = (int)$parent_category;
        else
            $this->parent_category = null;
        return $this;
    }
}