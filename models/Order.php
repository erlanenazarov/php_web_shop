<?php
/**
 * Created by PhpStorm.
 * User: erlan
 * Date: 17.05.2018
 * Time: 17:54
 */

class Order {
    public $id;

    public $user;

    public $is_checked = false;

    public $items = [];

    public function __construct($id, $user, $is_checked) {
        $this->id = $id;
        $this->user = $user;
        $this->is_checked = $is_checked;

        $orderItemService = new OrderItemService();
        $this->items = $orderItemService->getOrderItems($id);

        return $this;
    }
}