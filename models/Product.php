<?php
/**
 * Created by PhpStorm.
 * User: erlan
 * Date: 06.05.2018
 * Time: 14:50
 */

class Product {

    public $id;

    public $title;

    public $slug;

    public $image;

    public $shortDescription;

    public $fullDescription;

    public $rating;

    public $category;

    public $is_active;

    public $price;

    public function __construct(
        $id,
        $title,
        $slug,
        $image,
        $shortDescription,
        $fullDescription,
        $rating,
        $category,
        $is_active,
        $price
    ) {
        $this->id = $id;
        $this->title = $title;
        $this->slug = $slug;
        $this->image = $image;
        $this->shortDescription = $shortDescription;
        $this->fullDescription = $fullDescription;
        $this->rating = $rating;
        $this->category = $category;
        $this->is_active = $is_active;
        $this->price = $price;

        return $this;
    }
}