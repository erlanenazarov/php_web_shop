<?php
/**
 * Created by PhpStorm.
 * User: erlan
 * Date: 15.03.2018
 * Time: 11:40
 */

class Auth extends Controller
{
    public function login($data = null) {
        $security = new Security();
        $result = $security->validateUser($data['email'], $data['password']);

        header('Content-Type: application/json');
        echo(json_encode($result));
    }

    public function register($data = null) {
        //header('Content-Type: application/json');
        if ($data['password'] === $data['re-password']) {
            $password = md5($data['re-password']);
            $security = new Security();
            if ($security->validateUser($data['email'], $data['password'])['result'] === 'fail') {
                $service = new UserService();
                $result = $service->insertObjects(
                    "nickName, email, password, cart_id, role",
                    "'{$data['nickName']}', '{$data['email']}', '{$password}', null, 'user'"
                );

                echo (json_encode($result));
            } else {
                echo (json_encode(array(
                    'success' => false,
                    'message' => 'Пользователь с таким E-Mail\'ом уже зарегистрирован'
                )));
            }
        } else {
            echo (array(
                'success' => false,
                'message' => 'Пароли не совпадают!'
            ));
        }
    }

    public function profile($data = null) {
        if (!Security::getInstance()->isAuth()) {
            header('Location: /auth/login/');
        }

        $params = array(
            'user' => Security::getInstance()->getUser()
        );

        View::render('profile.html', array_merge(self::generateViewParams(), $params));
    }
}