<?php
/**
 * Created by PhpStorm.
 * User: erlan
 * Date: 07.05.2018
 * Time: 17:59
 */

class Cart extends Controller {


    public function basket_list($data) {
        Security::getInstance()->checkForAuth();

        $user = Security::getInstance()->getUser();

        $service = new CartService();
        $basket = $service->getUserBasket($user->id);

        $params = array(
            'basket' => $basket,
        );
        View::render('basket.html', array_merge($params, self::generateViewParams()));
    }

    public function add_to_basket($data = null) {
        header('Content-Type: application/json');
        if (!Security::getInstance()->isAuth()) {
            echo json_encode(array(
                'success' => false,
                'message' => 'Что бы пополнить свою корзину вы должны авторизоваться!'
            ));
            return;
        }

        if (!isset($data['slug'])) {
            echo json_encode(array(
                'success' => false,
                'message' => 'Товар, который вы хотите добавить не найден!'
            ));
            return;
        }

        $productService = new ProductService();
        $cartService = new CartService();
        $cartItemService = new CartItemService();

        $product = $productService->getBySlug($data['slug']);
        $cart = $cartService->getUserBasket(Security::getInstance()->getUser()->id);


        $item = $cartItemService->filter(array(
            'cart_id' => $cart['id'],
            'product_id' => $product->id
        ));

        $result = null;

        if ($item !== null) {
            $updating = $cartItemService->updateObject(
                array(
                    'quantity' => (int)$item[0]['quantity'] + (int)$_REQUEST['count']
                ),
                array(
                    'id' => $item[0]['id']
                )
            );

            if ($updating) {
                $result = array(
                    'success' => true,
                    'message' => 'Обновлено количество товара в корзине!',
                    'mode' => 'update'
                );
            } else {
                $result = array(
                    'success' => false,
                    'message' => 'Произошла неизвестная ошибка!'
                );
            }
        } else {
            $inserting = $cartItemService->insertObjects(
                'cart_id, product_id, quantity',
                "'{$cart['id']}', '{$product->id}', '{$_REQUEST['count']}'"
            );

            if ($inserting['success']) {
                $result = array(
                    'success' => true,
                    'message' => 'Товар добавлен в корзину!',
                    'mode' => 'add'
                );
            } else {
                $result = array(
                    'success' => false,
                    'message' => 'Произошла неизвестная ошибка!'
                );
            }
        }

        echo json_encode($result);
    }


    public function update_basket_item($data = null) {
        header('Content-Type: application/json');
        if (!Security::getInstance()->isAuth()) {
            echo json_encode(array(
                'success' => false,
                'message' => 'Что бы обновить свою корзину вы должны авторизоваться!'
            ));
            return;
        }

        if (!isset($data['c_i_id'])) {
            echo json_encode(array(
                'success' => false,
                'message' => 'Товар, который вы хотите обновить не найден!'
            ));
            return;
        }


        $cartItemService = new CartItemService();
        $item = $cartItemService->get($data['c_i_id']);

        if ($item !== null) {
            $updating = $cartItemService->updateObject(
                array(
                    'quantity' => $_REQUEST['count']
                ),
                array(
                    'id' => $item['id']
                )
            );

            if ($updating) {
                echo json_encode(array(
                    'success' => true,
                    'message' => 'Количество товара обновлено!',
                    'sub_total' => (int)$item['product']->price * (int)$_REQUEST['count'],
                ));
                return;
            } else {
                echo json_encode(array(
                    'success' => false,
                    'message' => 'Произошла неизвестная ошибка!'
                ));
                return;
            }
        }

        echo json_encode(array(
            'success' => false,
            'message' => 'Продкут в корзине не найден!'
        ));
    }

    public function remove_from_basket($data = null) {
        header('Content-Type: application/json');
        if (!Security::getInstance()->isAuth()) {
            echo json_encode(array(
                'success' => false,
                'message' => 'Что бы обновить свою корзину вы должны авторизоваться!'
            ));
            return;
        }

        if (!isset($data['id'])) {
            echo json_encode(array(
                'success' => false,
                'message' => 'Товар, который нужно удалить не удалось найти'
            ));
            return;
        }

        $service = new CartItemService();
        $result = $service->deleteObjects(array(
            'id' => $data['id']
        ));

        if ($result) {
            echo json_encode(array(
                'success' => true,
                'message' => 'Продукт удален из корзины'
            ));
            return;
        }

        echo json_encode(array(
            'success' => false,
            'message' => 'Произошла неизвестная ошибка!'
        ));
    }

    public function get_basket_total_price($data = null) {
        header('Content-Type: application/json');
        if (!Security::getInstance()->isAuth()) {
            echo json_encode(array(
                'success' => false,
                'message' => 'Что бы обновить свою корзину вы должны авторизоваться!'
            ));
            return;
        }

        $user = Security::getInstance()->getUser();

        $service = new CartService();
        $basket = $service->getUserBasket($user->id);

        echo json_encode(array(
            'total' => $service->getBasketTotalPrice($basket['items']),
            'success' => true
        ));
    }

}