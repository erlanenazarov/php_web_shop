<?php
/**
 * Created by PhpStorm.
 * User: erlan
 * Date: 13.03.2018
 * Time: 18:26
 */

class Index extends Controller {

    /**
     *
     */
    function index_action() {

        $productService = new ProductService();


        $params = array(
            'products' => $productService->filter(array('is_active' => 't'), array('rating' => 'DESC'))
        );
        View::render('index.html', array_merge($params, self::generateViewParams()));
    }
}