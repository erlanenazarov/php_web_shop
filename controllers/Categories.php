<?php
/**
 * Created by PhpStorm.
 * User: erlan
 * Date: 07.05.2018
 * Time: 16:37
 */

class Categories extends Controller {

    public function first_level_category($data) {
        if (!isset($data['slug'])) return ErrorHandler::ConvertError('404');

        $categoryService = new CategoryService();
        $productService = new ProductService();

        $category = $categoryService->getBySlug($data['slug']);
        $sub_categories = $categoryService->filter(array(
            'parent_category' => $category->id
        ));

        $ids = array(
            $category->id
        );

        foreach ($sub_categories as $cat) {
            array_push($ids, $cat->id);
        }

        $products = $productService->filter(
            array(
                'category_id' => $ids
            ),
            array('rating' => 'DESC')
        );

        $params = array(
            'products' => $products,
            'category' => $category
        );
        View::render('category.html', array_merge($params, self::generateViewParams()));
    }

    public function second_level_category($data) {
        if (!isset($data['p_slug']) || !isset($data['c_slug'])) return ErrorHandler::ConvertError('404');

        $categoryService = new CategoryService();
        $productService = new ProductService();

        $category = $categoryService->getBySlug($data['c_slug']);
        $products = $productService->filter(array('category_id' => $category->id));

        $params = array(
            'products' => $products,
            'category' => $category
        );
        View::render('category.html', array_merge($params, self::generateViewParams()));
    }

}