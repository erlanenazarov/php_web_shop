<?php
/**
 * Created by PhpStorm.
 * User: erlan
 * Date: 16.03.2018
 * Time: 15:45
 */

class Admin extends Controller
{
    public function login($data = null) {
//        if (Security::getInstance()->isAuth()) {
//            if (isset($data['next']))
//                header('Location: '.$data['next']);
//            else
//                header('Location: /');
//        }

        $params = array();

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $email = $_REQUEST['email'];
            $password = $_REQUEST['password'];

            $result = Security::getInstance()->validateUser($email, $password);
            if ($result['result']) {
                if (isset($data['next']))
                    header('Location: '.$data['next']);
                else
                    header('Location: /');
            } else {
                $params['error'] = $result['reason'];
            }
        }
        View::render('admin/login.html', $params);
    }

    public function logout($data = null) {
        Security::getInstance()->removeSession();
        if (isset($data['next']))
            header('Location: '.$data['next']);
        else
            header('Location: /');
    }

    public function index_action($data = null) {
        Security::getInstance()->checkAccess();
        $categoryService = new CategoryService();
        $all_categories = $categoryService->getAll();

        $params = array(
            'location' => 'categories',
            'categories' => $all_categories
        );
        View::render('admin/index.html', array_merge($params, $data));
    }

    public function category_create($data = null) {
        Security::getInstance()->checkAccess();
        $categoryService = new CategoryService();
        $categories = $categoryService->getAll();

        $params = array(
            'categories' => $categories,
            'location' => 'categories'
        );
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $categoryObject = array(
                'title' => $_REQUEST['title'],
                'slug' => $_REQUEST['slug'],
                'parent_category' => $_REQUEST['parent_category'] === "" ? 'null' : $_REQUEST['parent_category']
            );

            var_dump($categoryObject);

            if ($categoryObject['title'] !== null && $categoryObject['slug']) {
                if ($categoryObject['parent_category'] === '') {
                    $categoryObject['parent_category'] = 'null';
                }
                $result = $categoryService->insertObjects(
                    "title, slug, parent_category",
                    "'{$categoryObject['title']}', '{$categoryObject['slug']}', {$categoryObject['parent_category']}"
                );

                if ($result['success']) {
                    $params['result'] = array(
                        'success' => true,
                        'message' => 'Категория добавлена'
                    );
                } else {
                    $params['result'] = array(
                        'success' => false,
                        'message' => 'Произошла ошибка при создании категории'
                    );
                }
            } else {
                $params['result'] = array(
                    'message' => 'Поле title или slug не заполнены!'
                );
            }
        }

        View::render('admin/category/add.html', $params);
    }

    public function category_edit($data = null) {
        Security::getInstance()->checkAccess();
//        die('Your are in category_edit action');
        if (!isset($data['id'])) return ErrorHandler::ConvertError(404);

        $service = new CategoryService();
        $category = $service->get($data['id']);

        $params = array(
            'category' => $category,
            'location' => 'categories',
            'categories' => $service->getAll()
        );

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $parent_category = $_REQUEST['parent_category'];
            $title = $_REQUEST['title'];
            $slug = $_REQUEST['slug'];
            if ($parent_category === "") $parent_category = 'NULL'; else $parent_category = (int)$parent_category;
            $result = $service->updateObject(
                array(
                    'title' => $title,
                    'slug' => $slug,
                    'parent_category' => $parent_category
                ),
                array(
                    'id' => $data['id']
                )
            );

            if ($result) {
                $params['result'] = array(
                    'success' => true,
                    'message' => 'Категория обновлена'
                );
                $params['category'] = $service->get($data['id']);
            } else {
                $params['result'] = array(
                    'success' => false,
                    'message' => $service->database->get_error($service->database->connect())
                );
            }
        }

        View::render('admin/category/edit.html', $params);
    }

    public function category_delete($data = null) {
        Security::getInstance()->checkAccess();

        if (!isset($data['id'])) return ErrorHandler::ConvertError(404);

        $service = new CategoryService();

        if ($service->delete($data['id']))
            header('Location: /admin/?success');
        else
            header('Location: /admin/?error');
    }

    public function users_dashboard($data = null) {
        Security::getInstance()->checkAccess();

        $userService = new UserService();
        $users = $userService->getAll();

        $params = array(
            'location' => 'users',
            'users' => $users
        );
        View::render('admin/users.dashboard.html', array_merge($params, $data));
    }

    public function users_create($data = null) {
        Security::getInstance()->checkAccess();
        $userService = new UserService();

        $params = array(
            'location' => 'users'
        );
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if ($data['email'] !== "" && $data['password'] !== "") {
                $userObject = array(
                    'nickname' => $_REQUEST['nickname'],
                    'email' => $_REQUEST['email'],
                    'password' => md5($_REQUEST['password']),
                    'cart_id' => 'NULL',
                    'role' => 'user',
                    'is_superuser' => $_REQUEST['is_superuser'] !== ""
                );

                $result = $userService->insertObjects(
                    "nickname, email, password, role, is_superuser",
                    "'{$userObject['nickname']}', '{$userObject['email']}', '{$userObject['password']}', '{$userObject['role']}', '{$userObject['is_superuser']}'"
                );

                $params['result'] = $result;
            } else {
                $params['result'] = array(
                    'success' => false,
                    'message' => 'Поля E-Mail и Пароль не заполнены'
                );
            }
        }

        View::render('admin/users/add.html', $params);
    }

    public function users_edit($data = null) {
        Security::getInstance()->checkAccess();

        if (!isset($data['id'])) return ErrorHandler::ConvertError(404);

        $service = new UserService();
        $user = $service->get($data['id']);

        $params = array(
            'user' => $user,
            'location' => 'users',
        );

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $nickname = $_REQUEST['nickname'];
            $email = $_REQUEST['email'];
            $isSuperuser = isset($_REQUEST['is_superuser']) ? 't' : 'f';

            $result = $service->updateObject(
                array(
                    'nickname' => $nickname,
                    'email' => $email,
                    'is_superuser' => $isSuperuser
                ),
                array(
                    'id' => $user->id
                )
            );

            if ($result) {
                $params['result'] = array(
                    'success' => true,
                    'message' => 'Пользователь обновлен'
                );
                $params['user'] = $service->get($data['id']);
            } else {
                $params['result'] = array(
                    'success' => false,
                    'message' => $service->database->get_error($service->database->connect())
                );
            }
        }

        View::render('admin/users/edit.html', $params);
    }

    public function users_change_password($data = null) {
        Security::getInstance()->checkAccess();

        if (!isset($data['id'])) return ErrorHandler::ConvertError(404);

        $service = new UserService();
        $user = $service->get($data['id']);

        $params = array(
            'location' => 'users',
            'user' => $user
        );

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $password1 = $_REQUEST['password1'];
            $password2 = $_REQUEST['password2'];

            if ($password1 && $password2 && $password1 === $password2) {

                $result = $service->updateObject(
                    array(
                        'password' => md5($password2)
                    ),
                    array(
                        'id' => $user->id
                    )
                );

                if ($result) {
                    $params['result'] = array(
                        'success' => true,
                        'message' => 'Пароль изменен'
                    );
                    header('Location: /admin/users/'.$user->id.'/change/?success');
                    exit;
                } else {
                    $params['result'] = array(
                        'success' => false,
                        'message' => $service->database->get_error($service->database->connect())
                    );
                }

            } else {
                $params['result'] = array(
                    'success' => false,
                    'message' => 'Пароли не совпадают'
                );
            }
        }

        View::render('admin/users/change.password.html', $params);
    }

    public function users_delete($data = null) {
        Security::getInstance()->checkAccess();

        if (!isset($data['id'])) return ErrorHandler::ConvertError(404);

        $service = new UserService();

        if ($service->delete($data['id'])) {
            header('Location: /admin/users/?success');
            exit;
        } else {
            header('Location: /admin/users/?error');
            exit;
        }
    }

    public function products_dashboard($data = null) {
        Security::getInstance()->checkAccess();

        $service = new ProductService();
        $products = $service->getAll();

        $params = array(
            'location' => 'Products',
            'Products' => $products
        );

        View::render('admin/product.dashboard.html', $params);
    }

    public function products_create($data = null) {
        Security::getInstance()->checkAccess();

        $service = new ProductService();
        $categoryService = new CategoryService();

        $params = array(
            'location' => 'Products',
            'categories' => $categoryService->getAll()
        );

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $imageUploading = null;
            if (isset($_FILES['image']) && !is_null($_FILES['image'])) {
                $imageUploading = $service->uploadFile($_FILES['image'], 'Products');
            }

            if (!is_null($imageUploading) && $imageUploading['success']) {
                $is_active = isset($_REQUEST['is_active']) ? 't' : 'f';
                $result = $service->insertObjects(
                    "title, slug, image_path, short_description, full_description, rating, category_id, is_active, price",
                    "'{$_REQUEST['title']}'," .
                    " '{$_REQUEST['slug']}'," .
                    " '{$imageUploading['fileName']}'," .
                    " '{$_REQUEST['short_description']}'," .
                    " '{$_REQUEST['full_description']}'," .
                    " '{$_REQUEST['rating']}'," .
                    " '{$_REQUEST['category_id']}'," .
                    " '{$is_active}'," .
                    " '{$_REQUEST['price']}'"
                );

                if ($result['success']) {
                    $params['result'] = array(
                        'success' => true,
                        'message' => 'Товар добавлен'
                    );
                } else {
                    $params['result'] = $result;
                }
            } else {
                $params['result'] = $imageUploading;
            }
        }

        View::render('admin/products/add.html', $params);
    }

    public function products_edit($data = null) {
        Security::getInstance()->checkAccess();

        if (!isset($data['id'])) return ErrorHandler::ConvertError(404);

        $service = new ProductService();
        $categoryService = new CategoryService();

        $product = $service->get($data['id']);

        $params = array(
            'location' => 'Products',
            'product' => $product,
            'categories' => $categoryService->getAll()
        );

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $image = null;
            if ($_FILES['image']['error'] === 0) {
                $image = $service->uploadFile($_FILES['image'], 'Products');
            }

            $title = $_REQUEST['title'];
            $slug = $_REQUEST['slug'];
            $shortDescription = $_REQUEST['short_description'];
            $fullDescription = $_REQUEST['full_description'];
            $rating = $_REQUEST['rating'];
            $category_id = $_REQUEST['category_id'];
            $is_active = isset($_REQUEST['is_active']) ? 't' : 'f';
            $price = $_REQUEST['price'];

            $changes = array(
                'title' => $title,
                'slug' => $slug,
                'short_description' => $shortDescription,
                'full_description' => $fullDescription,
                'rating' => $rating,
                'category_id' => $category_id,
                'is_active' => $is_active,
                'price' => $price
            );

            if ($image !== null) {
                $changes['image_path'] = $image['fileName'];
            }

            $result = $service->updateObject(
                $changes,
                array(
                    'id' => $product->id
                )
            );

            if ($result) {
                $params['result'] = array(
                    'success' => true,
                    'message' => 'Товар обновлен'
                );
                $params['product'] = $service->get($data['id']);
            } else {
                $params['result'] = array(
                    'success' => false,
                    'message' => $service->database->get_error($service->database->connect())
                );
            }
        }

        View::render('admin/products/edit.html', $params);
    }
}