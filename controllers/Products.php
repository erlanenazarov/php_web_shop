<?php
/**
 * Created by PhpStorm.
 * User: erlan
 * Date: 07.05.2018
 * Time: 15:12
 */

class Products extends Controller {


    public function product_single($data = null) {
        if (!isset($data['slug'])) return ErrorHandler::ConvertError('404');

        $productService = new ProductService();
        $product = $productService->getBySlug($data['slug']);

        $params = array(
            'product' => $product
        );

        View::render('product.single.html', array_merge($params, self::generateViewParams()));
    }

}