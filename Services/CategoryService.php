<?php
/**
 * Created by PhpStorm.
 * User: erlan
 * Date: 16.03.2018
 * Time: 16:05
 */

class CategoryService extends EntityManager
{
    public function __construct()
    {
        parent::__construct();
        $this->table = 'categories';
    }

    private function is_category_null($cat) {
        return $cat !== "" || is_null($cat);
    }

    public function getAll() {
        $result = array();

        $categories = $this->getObjects('*');
        for($i=0; $i < sizeof($categories); ++$i) {
            $cat = $categories[$i];
            $result[$i] = new Category(
                $cat['id'],
                $cat['title'],
                $cat['slug'],
                $cat['parent_category']
            );
        }

        return $result;
    }

    public function get($id) {
        $result = null;

        $category = $this->getObjects('*', array('id' => $id));
        if($category[0]) {
            $result = $category[0];
            return new Category($result['id'], $result['title'], $result['slug'], $result['parent_category']);
        }

        return null;
    }

    public function getBySlug($slug) {
        $result = null;

        $category = $this->getObjects('*', array('slug' => $slug));
        if($category[0]) {
            $result = $category[0];
            if ($result['parent_category'] !== null) {
                $result['parent_category'] = $this->get($result['parent_category']);
            }
            return new Category($result['id'], $result['title'], $result['slug'], $result['parent_category']);
        }

        return null;
    }

    public function delete($id) {
        return $this->deleteObjects(array('id' => $id));
    }

    public function filter($filters) {
        $result = array();

        $categories = $this->getObjects('*', $filters);
        for ($i=0; $i < sizeof($categories); ++$i) {
            $cat = $categories[$i];
            $result[$i] = new Category(
                $cat['id'],
                $cat['title'],
                $cat['slug'],
                $cat['parent_category']
            );
        }

        return $result;
    }

}