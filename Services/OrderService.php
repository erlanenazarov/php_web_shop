<?php
/**
 * Created by PhpStorm.
 * User: erlan
 * Date: 08.05.2018
 * Time: 0:24
 */

class OrderService extends EntityManager {
    function __construct() {
        parent::__construct();
        $this->table = 'orders';
    }

    public function getAll() {
        $result = array();

        $userService = new UserService();

        $orders = $this->getObjects('*');
        for($i=0; $i < sizeof($orders); ++$i) {
            $order = $orders[$i];

            if ($order['user_id'] !== null) {
                $order['user'] = $userService->get($order['user_id']);
            } else {
                $order['user'] = null;
            }

            $result[$i] = new Order(
                $order['id'],
                $order['user'],
                $order['is_checked']
            );
        }

        return $result;
    }
}