<?php
/**
 * Created by PhpStorm.
 * User: erlan
 * Date: 07.05.2018
 * Time: 18:23
 */

class CartItemService extends EntityManager {

    public function __construct()
    {
        parent::__construct();
        $this->table = 'cart_item';
    }


    public function filter($filters) {
        $result = array();

        $productService = new ProductService();

        $items = $this->getObjects('*', $filters, array(
            'id' => 'DESC'
        ));
        if ($items[0] == null) return null;
        for ($i=0; $i < sizeof($items); ++$i) {
            $p = $items[$i];
            $p['product'] = $productService->get($p['product_id']);
            $p['sub_total'] = (int)$p['product']->price * (int)$p['quantity'];
            $result[$i] = $p;
        }

        return $result;
    }

    public function get($id) {
        $item = $this->getObjects('*', array('id' => $id));
        if ($item[0]) {
            $cartItem = $item[0];
            $productService = new ProductService();

            $cartItem['product'] = $productService->get($cartItem['product_id']);
            return $cartItem;
        }

        return null;
    }
}