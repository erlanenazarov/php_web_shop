<?php
/**
 * Created by PhpStorm.
 * User: erlan
 * Date: 15.03.2018
 * Time: 10:13
 */

class UserService extends EntityManager
{
    function __construct()
    {
        parent::__construct();
        $this->table = 'users';
    }


    public function getAll() {
        $result = array();
        $users = $this->getObjects('*');
        for ($i=0; $i < sizeof($users); ++$i) {
            $result[$i] = new User(
                $users[$i]['id'],
                $users[$i]['nickname'],
                $users[$i]['email'],
                $users[$i]['password'],
                $users[$i]['role'],
                $users[$i]['is_superuser'] == 't' ? true : false,
                $users['address'],
                $users['phone']
            );
        }
        return $result;
    }

    function getUserByEmail($email) {
        $data = $this->getObjects('*', array(
            'email' => $email
        ));
        if ($data[0] != null) {
            return new User(
                $data[0]['id'],
                $data[0]['nickName'],
                $data[0]['email'],
                $data[0]['password'],
                $data[0]['cart_id'],
                $data[0]['role'],
                $data[0]['is_superuser']
            );
        }
        return null;
    }

    public function filter($filters) {
        $result = array();
        $users = $this->getObjects('*', $filters);
        for ($i=0; $i < sizeof($users); ++$i) {
            $result[$i] = new User(
                $users[$i]['id'],
                $users[$i]['nickname'],
                $users[$i]['email'],
                $users[$i]['password'],
                $users[$i]['role'],
                $users[$i]['is_superuser'],
                $users['address'],
                $users['phone']
            );
        }
        return $result;
    }

    public function get($id) {
        $result = null;

        $user = $this->getObjects('*', array('id' => $id));
        if($user[0]) {
            $result = $user[0];

            if (DB_CORE === 'postgres') {
                $is_superuser = $result['is_superuser'] === 't';
            } else {
                $is_superuser = $result['is_superuser'] === 1 || $result['is_superuser'] === '1';
            }

            return new User(
                $result['id'],
                $result['nickname'],
                $result['email'],
                $result['password'],
                $result['role'],
                $is_superuser,
                $result['address'],
                $result['phone']
            );
        }

        return null;
    }


    public function delete($id) {
        return $this->deleteObjects(array('id' => $id));
    }
}