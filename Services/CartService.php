<?php
/**
 * Created by PhpStorm.
 * User: erlan
 * Date: 07.05.2018
 * Time: 18:04
 */

class CartService extends EntityManager {

    public function __construct()
    {
        parent::__construct();
        $this->table = 'cart';
    }


    public function getUserBasket($id) {
        // $result = null;
        $cart = $this->getObjects('*', array('user_id' => $id, 'is_done' => 'false'));
        if ($cart[0]) {
            $basket = $cart[0];
            if (is_array($cart) && sizeof($cart) > 1) $basket = $cart[sizeof($cart)-1];

            $cartItemService = new CartItemService();
            $cartItems = $cartItemService->filter(array(
                'cart_id' => $basket['id']
            ));

            return array(
                'id' => $basket['id'],
                'user_id' => $basket['user_id'],
                'items' => $cartItems,
                'total' => $this->getBasketTotalPrice($cartItems)
            );
        }

        return $this->createUserBasket($id);
    }

    public function getBasketTotalPrice($items) {
        $total = 0;

        if ($items !== null) {
            foreach ($items as $item) {
                $total += $item['sub_total'];
            }
        }

        return $total;
    }

    public function createUserBasket($id) {
        $result = $this->insertObjects('user_id', "'{$id}'");

        if ($result['success']) {
            return array(
                'id' => $result['id'],
                'user_id' => $id,
                'is_done' => false,
                'items' => null,
                'total' => 0
            );
        }

        return null;
    }

}