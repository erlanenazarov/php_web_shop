<?php
/**
 * Created by PhpStorm.
 * User: erlan
 * Date: 06.05.2018
 * Time: 14:49
 */

class ProductService extends EntityManager {

    function __construct() {
        parent::__construct();
        $this->table = 'Products';
    }

    public function getAll () {
        $result = array();


        $products = $this->getObjects('*');
        if ($products[0] !== false) {
            for ($i=0; $i < sizeof($products); ++$i) {
                $p = $products[$i];
                $p['image'] = array(
                    'name' => $p['image_path'],
                    'url' => '/media/products/'.$p['image_path']
                );
                $categoryService = new CategoryService();
                $category = $categoryService->get($p['category_id']);
                if ($category->parent_category !== null) {
                    $category->parent_category = $categoryService->get($category->parent_category);
                }
                $p['category'] = $category;
                $result[$i] = new Product(
                    $p['id'],
                    $p['title'],
                    $p['slug'],
                    $p['image'],
                    $p['short_description'],
                    $p['full_description'],
                    $p['rating'],
                    $p['category'],
                    $p['is_active'] == 't' ? true : false,
                    $p['price']
                );
            }
        }

        return $result;
    }

    public function filter($filters, $sort = array()) {
        $result = array();


        $products = $this->getObjects('*', $filters, $sort);
        if ($products[0] == null) return null;
        for ($i=0; $i < sizeof($products); ++$i) {
            $p = $products[$i];
            $p['image'] = array(
                'name' => $p['image_path'],
                'url' => '/media/products/'.$p['image_path']
            );
            $categoryService = new CategoryService();
            $category = $categoryService->get($p['category_id']);
            if ($category->parent_category !== null) {
                $category->parent_category = $categoryService->get($category->parent_category);
            }
            $p['category'] = $category;
            $result[$i] = new Product(
                $p['id'],
                $p['title'],
                $p['slug'],
                $p['image'],
                $p['short_description'],
                $p['full_description'],
                $p['rating'],
                $p['category'],
                $p['is_active'] == 't' ? true : false,
                $p['price']
            );
        }

        return $result;
    }

    public function get($id) {
        $result = null;

        $product = $this->getObjects('*', array('id' => $id));
        if($product[0]) {
            $p = $product[0];
            $p['image'] = array(
                'name' => $p['image_path'],
                'url' => '/media/products/'.$p['image_path']
            );
            $categoryService = new CategoryService();
            $category = $categoryService->get($p['category_id']);
            if ($category->parent_category !== null) {
                $category->parent_category = $categoryService->get($category->parent_category);
            }
            $p['category'] = $category;

            return new Product(
                $p['id'],
                $p['title'],
                $p['slug'],
                $p['image'],
                $p['short_description'],
                $p['full_description'],
                $p['rating'],
                $p['category'],
                $p['is_active'] == 't' ? true : false,
                $p['price']
            );
        }

        return null;
    }

    public function getBySlug($slug) {
        $result = null;

        $product = $this->getObjects('*', array('slug' => $slug));
        if($product[0]) {
            $p = $product[0];
            $p['image'] = array(
                'name' => $p['image_path'],
                'url' => '/media/products/'.$p['image_path']
            );
            $categoryService = new CategoryService();
            $category = $categoryService->get($p['category_id']);
            if ($category->parent_category !== null) {
                $category->parent_category = $categoryService->get($category->parent_category);
            }
            $p['category'] = $category;

            return new Product(
                $p['id'],
                $p['title'],
                $p['slug'],
                $p['image'],
                $p['short_description'],
                $p['full_description'],
                $p['rating'],
                $p['category'],
                $p['is_active'] == 't' ? true : false,
                $p['price']
            );
        }

        return null;
    }

    public function uploadFile($file, $folder) {
        $dir = PROJECT_PATH.'/media/'.$folder.'/';
        $imageFileType = strtolower(pathinfo($file['name'],PATHINFO_EXTENSION));
        $newFileName = $this->generateRandomString();
        $newFileName = $newFileName.'.'.$imageFileType;
        $target_file = $dir.$newFileName;

        $check = getimagesize($file["tmp_name"]);

        if (!$check) {
            return array(
                'success' => false,
                'message' => 'Файл не определяется'
            );
        }

        if ($file['size'] > 500000) {
            return array(
                'success' => false,
                'message' => 'Размер файла превышает лимит!'
            );
        }

        if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
            && $imageFileType != "gif") {
            return array(
                'success' => false,
                'message' => 'Формат файла не поддерживается!'
            );
        }

        if (move_uploaded_file($file['tmp_name'], $target_file)) {
            return array(
                'success' => true,
                'message' => 'Файл загружен',
                'fileName' => $newFileName
            );
        } else {
            return array(
                'success' => false,
                'message' => 'Произошла неизвестная ошибка!'
            );
        }
    }

    public function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}