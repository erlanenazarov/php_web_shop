<?php
/**
 * Created by PhpStorm.
 * User: erlan
 * Date: 08.05.2018
 * Time: 0:25
 */

class OrderItemService extends EntityManager {
    function __construct() {
        parent::__construct();
        $this->table = 'order_item';
    }


    /**
     * Takes an order id
     * @param $id - Order ID
     * @return array - Returns array of all products in order
     */
    public function getOrderItems($id) {
        $productService = new ProductService();

        $result = [];

        $orderItems = $this->getObjects('*', array('order_id' => $id));
        if ($orderItems[0] !== null) {
            for($i = 0; $i < sizeof($orderItems); ++$i) {
                $item = $orderItems[$i];

                if ($item['product_id'] !== null) {
                    $item['product'] = $productService->get($item['product_id']);
                } else {
                    $item['product'] = null;
                }

                $result[$i] = array(
                    'id' => $item['id'],
                    'product' => $item['product'],
                    'quantity' => $item['quantity']
                );
            }
        }

        return $result;
    }
}