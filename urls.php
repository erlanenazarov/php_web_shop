<?php
/**
 * Created by PhpStorm.
 * User: erlan
 * Date: 17.03.2018
 * Time: 13:44
 */


function getUrls() {
    return array(
        url(
            '^/$',
            array(
                'controller' => 'Index',
                'action' => 'index_action'
            ),
            'Index'
        ),
        url(
            '^/admin/$',
            array(
                'controller' => 'Admin',
                'action' => 'index_action',
                'access' => 'superuser'
            ),
            'admin:index'
        ),
        url(
            '^/admin/category/create/$',
            array(
                'controller' => 'Admin',
                'action' => 'category_create',
                'access' => 'superuser'
            ),
            'admin:category:create'
        ),
        url(
            '^/admin/category/(?P<id>\d+)/change/$',
            array(
                'controller' => 'Admin',
                'action' => 'category_edit',
                'access' => 'superuser'
            ),
            'admin:category:edit'
        ),
        url(
            '^/auth/login/$',
            array(
                'controller' => 'Admin',
                'action' => 'login',
                'access' => 'global'
            ),
            'admin:login'
        ),
        url(
            '^/auth/logout/$',
            array(
                'controller' => 'Admin',
                'action' => 'logout'
            ),
            'admin:logout'
        ),
        url(
            '^/auth/register/$',
            array(
                'controller' => 'Admin',
                'action' => 'register'
            ),
            'admin:register'
        ),
        url(
            '^/admin/category/(?P<id>\d+)/delete/$',
            array(
                'controller' => 'Admin',
                'action' => 'category_delete'
            ),
            'admin:category:delete'
        ),
        url(
            '^/admin/users/$',
            array(
                'controller' => 'Admin',
                'action' => 'users_dashboard'
            ),
            'admin:users'
        ),
        url(
            '^/admin/users/create/$',
            array(
                'controller' => 'Admin',
                'action' => 'users_create'
            ),
            'admin:users:create'
        ),
        url(
            '^/admin/users/(?P<id>\d+)/change/$',
            array(
                'controller' => 'Admin',
                'action' => 'users_edit'
            ),
            'admin:users:change'
        ),
        url(
            '^/admin/users/(?P<id>\d+)/change/password/$',
            array(
                'controller' => 'Admin',
                'action' => 'users_change_password'
            ),
            'admin:users:password:change'
        ),
        url(
            '^/admin/users/(?P<id>\d+)/delete/$',
            array(
                'controller' => 'Admin',
                'action' => 'users_delete'
            ),
            'admin:users:delete'
        ),
        url(
            '^/admin/products/$',
            array(
                'controller' => 'Admin',
                'action' => 'products_dashboard'
            ),
            'admin:products'
        ),
        url(
            '^/admin/products/create/$',
            array(
                'controller' => 'Admin',
                'action' => 'products_create'
            ),
            'admin:products:create'
        ),
        url(
            '^/admin/products/(?P<id>\d+)/change/$',
            array(
                'controller' => 'Admin',
                'action' => 'products_edit'
            ),
            'admin:products:edit'
        ),
        url(
            '^/products/(?P<slug>[\w-]+)/$',
            array(
                'controller' => 'Products',
                'action' => 'product_single'
            ),
            'products:single'
        ),
        url(
            '^/categories/(?P<slug>[\w-]+)/$',
            array(
                'controller' => 'Categories',
                'action' => 'first_level_category'
            ),
            'categories:level:first'
        ),
        url(
            '^/categories/(?P<p_slug>[\w-]+)/(?P<c_slug>[\w-]+)/$',
            array(
                'controller' => 'Categories',
                'action' => 'second_level_category'
            ),
            'categories:level:second'
        ),
        url(
            '^/cart/$',
            array(
                'controller' => 'Cart',
                'action' => 'basket_list'
            ),
            'cart:list'
        ),
        url(
            '^/cart/product/(?P<slug>[\w-]+)/add/$',
            array(
                'controller' => 'Cart',
                'action' => 'add_to_basket'
            ),
            'cart:add'
        ),
        url(
            '^/cart/item/(?P<c_i_id>\d+)/update/$',
            array(
                'controller' => 'Cart',
                'action' => 'update_basket_item'
            ),
            'cart:update'
        ),
        url(
            '^/cart/total-price/get/$',
            array(
                'controller' => 'Cart',
                'action' => 'get_basket_total_price'
            ),
            'cart:get_total_price'
        ),
        url(
            '^/cart/(?P<id>\d+)/delete/$',
            array(
                'controller' => 'Cart',
                'action' => 'remove_from_basket'
            ),
            'cart:remove_from_basket'
        ),
        url(
            '^/auth/profile/$',
            array(
                'controller' => 'Auth',
                'action' => 'profile'
            ),
            'auth:profile'
        )
    );
}


function url($reg, $options, $name) {
    $key = preg_replace("/\//", '\/', $reg);
    return array(
        'url' => $key,
        'data' => $options,
        'name' => $name,
        'origin_url' => $reg
    );
}


/**
 *
 * Эта функция не дописана, так что она не работает!
 *
 * @param $url - Название URL, например 'categories:level:first'
 * @param null $data - Параметры которые принимает этот Эндпоинт, например array('slug' => 'laptops')
 * @return null|string|string[] - На выходе мы имеем сгенерированный URL, 'например /categories/laptops/'
 * @deprecated
 */
function reverse($url, $data = null) {
    $data['value'] = 2;
    $currentRoute = null;
    foreach (getUrls() as $key => $value) {
        if ($value['name'] == $url) {
            $currentRoute = $value;
        }
    }

    $path = preg_replace('(?P<(.+?)>\(.+?))', $data['value'], $currentRoute['origin_url']);

    return $path;
}
