<?php
/**
 * Created by PhpStorm.
 * User: erlan
 * Date: 5/27/17
 * Time: 1:50 PM
 */

class Controller {

    static function generateViewParams() {
        $categoryService = new CategoryService();

        $categories = $categoryService->filter(array('parent_category' => null));
        $prepared_categories = array();
        foreach ($categories as $cat) {
            $cat_object = array(
                'id' => $cat->id,
                'title' => $cat->title,
                'slug' => $cat->slug,
                'parent_category' => $cat->parent_category,
                'sub_categories' => array()
            );
            $sub_categories = $categoryService->filter(array(
                'parent_category' => $cat->id
            ));
            foreach($sub_categories as $sub_cat) {
                array_push($cat_object['sub_categories'], array(
                    'id' => $sub_cat->id,
                    'title' => $sub_cat->title,
                    'slug' => $sub_cat->slug,
                    'parent_category' => $sub_cat->parent_category
                ));
            }
            array_push($prepared_categories, $cat_object);
        }

        $basket = null;
        if (Security::getInstance()->isAuth()) {
            $cartService = new CartService();
            $basket = $cartService->getUserBasket(Security::getInstance()->getUser()->id);
        }

        return array(
            'categories' => $prepared_categories,
            'basket_count' => $basket !== null ? $basket['items'] !== null ? sizeof($basket['items']) : 0 : 0,
        );
    }
}