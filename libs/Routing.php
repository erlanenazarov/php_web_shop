<?php

/**
 * Created by PhpStorm.
 * User: erlan
 * Date: 5/25/17
 * Time: 3:36 PM
 */
class Routing
{
    /**
     * Current Instance
     * @var Routing
     */
    private static $instance;
    /**
     * Current controller name
     * @var string
     */
    public $controller;
    /**
     * Current action name
     * @var string
     */
    public $action;
    /**
     * Base project url
     * @var string
     */
    private $base_url;

    /**
     * Closed Construct function
     */
    public function __construct()
    {
        session_set_cookie_params(3600,"/");
        session_start();
        $this->base_url = isset($_SERVER['HTTPS']) ? 'https' : 'http' . "://" . $_SERVER['HTTP_HOST'] . '/';
        $tmp = explode('/', $_SERVER['REQUEST_URI']);
        $this->base_url .= $tmp[1];
    }

    /**
     * Closed clone function
     */
    protected function __clone()
    {
    }

    /**
     * Create Routing class instance
     * @return Routing
     */
    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new Routing();
        }
        return self::$instance;
    }

//    localhost/{controller name}/{action}
    public function processRoute()
    {
        Security::getInstance()->checkForUserContains();
        $matches = array();
        $currentRoute = null;
        foreach(getUrls() as $key => $value) {
            $server_url = $_SERVER['REQUEST_URI'];
            $data = array();
            if (strpos($server_url, '?') != false) {
                $b = preg_split('/[?]+/', $server_url);
                $server_url = $b[0];
                $b = $b[1];
                if (strpos($b, '&') != false) {
                    $all_params = preg_split('/[&]+/', $b);
                    foreach($all_params as $p) {
                        $_param_key = preg_split('/[=]+/', $p)[0];
                        $_param_value = preg_split('/[=]+/', $p)[1];
                        $data[$_param_key] = $_param_value;
                    }
                } else {
                    if (strpos($b, '=') != false) {
                        $param_key = preg_split('/[=]+/', $b)[0];
                        $param_value = preg_split('/[=]+/', $b)[1];
                        $data[$param_key] = $param_value;
                    }
                }
            }

            preg_match("/{$value['url']}/", $server_url, $matches);


            if (sizeof($matches) > 0) {
                $currentRoute = $value['data'];
                $currentRoute['params'] = $data;
                break;
            }
        }

        if (is_null($currentRoute))
            return ErrorHandler::ConvertError('404');

//        if (isset($currentRoute) && $currentRoute['access'] === 'superuser') {
//            if (!Security::getInstance()->getUser()->is_superuser) {
//                header('Location: /auth/login/?next='.$_SERVER['REQUEST_URI']);
//                return null;
//            }
//        }

        $this->controller = $currentRoute['controller'];
        $this->action     = $currentRoute['action'];

        if (!file_exists(CONTROLLERS_PATH . $this->controller . '.php')) {
            return ErrorHandler::ConvertError('404');
        }
        require_once CONTROLLERS_PATH . $this->controller . '.php';

        if (!class_exists($this->controller)) {
            return ErrorHandler::ConvertError('404');
        }
        $controllerClass = new $this->controller();

        if (!method_exists($controllerClass, $this->action)) {
            return ErrorHandler::ConvertError('404');
        }
        $this->cleanupRequest();

        $params = array_merge($matches, $currentRoute['params']);

        call_user_func(array($this->controller, $this->action), array_merge($params, array(
            'request' => array(
                'user' => Security::getInstance()->getUser()
            )
        )));
    }

    /**
     * Return current project url
     * @return string
     */
    public function getBaseUrl()
    {
        return $this->base_url;
    }

    public function isMethod($method)
    {
        if ($_SERVER['REQUEST_METHOD'] == $method) {
            return true;
        }
        return false;
    }

    public function cleanupRequest()
    {
        foreach ($_REQUEST as $key => $value) {
            $value = addslashes(trim($value));
            $value = htmlentities($value, ENT_QUOTES);
            unset($_REQUEST['controller'], $_REQUEST['action']);
            $_REQUEST[$key] = $value;
        }
    }
}