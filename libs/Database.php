<?php
/**
 * Created by PhpStorm.
 * User: erlan
 * Date: 5/25/17
 * Time: 4:11 PM
 */

class Database
{


    public function __construct()
    {
        return $this;
    }

    /**
     * @var Database Connection
     */
    private $connection = null;


    public function connect()
    {
        if ($this->connection == null) {
            switch (DB_CORE) {
                case 'postgres':
                    $this->connection = pg_connect('host='.DB_HOST.' port=5432 dbname='.DB_NAME.' user='.DB_USER.' password='.DB_PASS);
                    break;
                default:
                case 'mysql':
                    $this->connection = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
                    mysqli_set_charset($this->connection, 'UTF8');
                    break;
            }
        }

        return $this->connection;
    }

    function query($sql) {
        switch (DB_CORE) {
            case 'postgres':
                return pg_query($this->connect(), $sql);
                break;
            default:
            case 'mysql':
                return mysqli_query($this->connect(), $sql);
        }
    }

    function num_rows($query) {
        switch (DB_CORE) {
            case 'postgres':
                return pg_num_rows($query);
                break;
            default:
            case 'mysql':
                return mysqli_num_rows($query);
        }
    }

    function fetch_assoc($query) {
        switch (DB_CORE) {
            case 'postgres':
                return pg_fetch_assoc($query);
                break;
            default:
            case 'mysql':
                return mysqli_fetch_assoc($query);
        }
    }

    function get_error($connection) {
        switch (DB_CORE) {
            case 'postgres':
                return pg_errormessage($connection);
                break;
            default:
            case 'mysql':
                return mysqli_error($connection);
        }
    }

    public function migrate()
    {
        $migration_file_name = PROJECT_PATH . '/migrations/migration.sql';
        if (file_exists($migration_file_name)) {
            $templine = "";
            $handle = fopen($migration_file_name, "r");

            if ($handle) {
                while (($line = fgets($handle)) !== false) {
                    if (substr($line, 0, 2) == '--' || $line == '')
                        continue;

                    $templine .= $line;

                    if (substr(trim($line), -1, 1) == ';')
                    {
                        print ('Выполняется запрос: '.$templine);
                        $this->query($templine) or print('Ошибка в выполнении запроса \'<strong>' . $templine . '\': ' . $this->get_error($this->connection) . '<br /><br />');
                        print ('================== ok'.PHP_EOL);
                        $templine = '';
                    }
                }

                fclose($handle);
                return 'База обновлена';
            }
            return 'Не удалось открыть файл миграции...';
        }
        return 'Нет файла миграций';
    }
}