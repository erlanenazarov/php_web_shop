<?php
/**
 * Created by PhpStorm.
 * User: erlan
 * Date: 5/25/17
 * Time: 4:06 PM
 */

class Security {
    public $loginSession = 'user';

    static private $instance = null;

    static function getInstance() {
        if(self::$instance == null) {
            self::$instance = new Security();
        }
        return self::$instance;
    }

    function checkAccess() {
        if (!$this->isAuth() || !$this->getUser()->is_superuser) {
            header('Location: /auth/login/?next='.$_SERVER['REQUEST_URI']);
            exit;
        }
    }

    function checkForUserContains() {
        if ($this->isAuth()) {
            $entity = new UserService();
            $id = $this->getUser()->id;
            $data = $entity->get($id);
            if(is_null($data)) {
                $this->removeSession();
            } else {
                $data->is_anonymous = false;
                $this->createSession($data);
            }
            return !is_null($data);
        }
        return false;
    }

    function createSession($value) {
        $_SESSION[$this->loginSession] = $value;
        return true;
    }

    function removeSession() {
        unset($_SESSION[$this->loginSession]);
        return true;
    }

    function validateUser($email, $password) {
        $password = md5($password);
        $user = new UserService();
        $data = $user->filter(array(
            'email' => $email
        ));
        if($data[0] != null) {
            if($data[0]->password == $password) {
                $data[0]->is_anonymous = false;
                $this->authenticateUser($data[0]);
                return array('result' => true, 'message' => 'Авторизация прошла успешно, сессия создана');
            } else {
                return array('result' => false, 'reason' => 'Неверный пароль');
            }
        }
        return array('result' => 'fail', 'reason' => 'Пользователя с таким логином нет в базе');
    }

    private function authenticateUser($user) {
        $this->createSession($user);
    }

    public function isAuth() {
        return isset($_SESSION[$this->loginSession]);
    }

    public function getUser() {
        if (isset($_SESSION[$this->loginSession]))
            return $_SESSION[$this->loginSession];
        return null;
    }

    public function checkForAuth() {
        if (!$this->isAuth())
            header('Location: /auth/login/?next='.$_SERVER['REQUEST_URI']);
    }
}