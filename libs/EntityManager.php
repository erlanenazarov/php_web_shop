<?php
/**
 * Created by PhpStorm.
 * User: erlan
 * Date: 5/25/17
 * Time: 4:19 PM
 */

class EntityManager extends CRUD {

    public $table = '';
    public $database = null;


    public function __construct() {
        if($this->database == null) {
            $this->database = new Database();
        }
    }

    function getObjects($fields, $where = array(), $order = array())
    {
        $query = 'SELECT '.$fields.' FROM '.$this->table;
        if(sizeof($where) > 0) {
            $where = $this->prepareWhere($where);
            if($where != null) {
                $query .= ' WHERE '.$where;
            }
        }

        if (sizeof($order) > 0) {
            $order = $this->orderBy($order);
            if ($order != null) {
                $query .= ' ORDER BY ' .$order;
            }
        }

        $result = $this->database->query($query);
        $n = $this->database->num_rows($result);

        if($n > 1) {
            $data = array();
            for($i=0; $i < $n; ++$i) {
                $row = $this->database->fetch_assoc($result);
                $data[$i] = $row;
            }
            return $data;
        }

        return array($this->database->fetch_assoc($result));
    }

    function insertObjects($fields, $values)
    {
        $query = 'INSERT INTO '.$this->table.' ('.$fields.') VALUES('.$values.')';
        if (DB_CORE === 'postgres') $query .= 'RETURNING id';
        $result = $this->database->query($query);

        $r = $this->database->fetch_assoc($result);

        if ($r === null) {
            return array(
                'success' => false,
                'message' => 'Произошла неизвестная ошибка!'
            );
        }

        if (DB_CORE === 'postgres') {
            $id = $r['id'];
        } else {
            $re = $this->database->query('SELECT LAST_INSERT_ID()');
            $id = $this->database->fetch_assoc($re)[2];
        }

        return array(
            'success' => true,
            'message' => $this->database->get_error($this->database->connect()),
            'id' => $id
        );
    }

    function updateObject($fields = array(), $where = array())
    {
        $fields = $this->prepareWhere($fields);
        $where = $this->prepareWhere($where);
        $query = 'UPDATE '.$this->table.' SET '.$fields.' WHERE '.$where;
        $result = $this->database->query($query);
        return $result;
    }

    function deleteObjects($fields = array())
    {
        $fields = $this->prepareWhere($fields);
        $query = 'DELETE FROM '.$this->table.' WHERE '.$fields;
        $result = $this->database->query($query);
        return $result;
    }

    function prepareWhere($fields = array()) {
        if(!is_array($fields) || sizeof($fields) == 0) {
            return null;
        }

        $result = '';
        $index = 0;
        foreach($fields as $key=>$value) {
            if($value !== 'NULL' && !is_null($value)) {
                if(is_numeric($value)) {
                    $result .= $key . '=' . $value;
                } elseif (is_array($value)) {
                    if (DB_CORE === 'postgres') {
                        $array_of_ids = $this->array_to_pg_string($value);
                        $result .= $key . '= ANY(' . $array_of_ids . ')';
                    } else {
                        $array_of_ids = $this->array_to_msq_string($value);
                        $result .= $key . '= IN (' . $array_of_ids . ')';
                    }
                } else {
                    $result .= $key . '=\'' . $value . '\'';
                }
            } else {
                $result .= $key . ' IS NULL';
            }
            $index++;
            if($index < sizeof($fields)) {
                if (DB_CORE === 'postgres') {
                    $result .= ' AND ';
                } else {
                    $result .= ', ';
                }
            }
        }
        return $result;
    }


    function array_to_pg_string($array) {
        $result = '\'{';
        $index = 0;

        foreach ($array as $item) {
            $result .= $item;
            $index++;
            if ($index < sizeof($array)) $result .= ',';
        }

        $result .= '}\'::int[]';

        return $result;
    }

    function array_to_msq_string($array) {
        return implode("','",$array);
    }

    function orderBy($fields = array()) {
        if(!is_array($fields) || sizeof($fields) == 0) {
            return null;
        }

        $result = '';
        $index = 0;
        foreach ($fields as $key => $value) {
            if ($value !== 'NULL' && !is_null($value)) {
                $result .= $key . ' ' . $value;
            } else {
                continue;
            }
            $index++;
            if ($index < sizeof($fields)) $result .= ',';
        }

        return $result;
    }

    function findBy($fields = array()) {
        if(!is_array($fields)) {
            $fields = array($fields);
        }

        $result = '';
        foreach($fields as $key=>$value) {
            $result .= $key . ' LIKE \'%' . $value .'%\' ';
        }
        $query = 'SELECT * FROM ' . $this->table . ' WHERE ' . $result;
        $result = $this->database->query($query);
        $n = $this->database->num_rows($result);
        if($n > 1) {
            $data = array();
            for($i=0; $i < $n; ++$i) {
                $row = $this->database->fetch_assoc($result);
                $data[$i] = $row;
            }
            return $data;
        }

        return array($this->database->fetch_assoc($result));
    }

    function nakedQuery($query) {
        $result = $this->database->query($query);
        $n = $this->database->num_rows($result);
        $data = array();
        for($i=0; $i < $n; ++$i) {
            $row = $this->database->fetch_assoc($result);
            $data[$i] = $row;
        }
        return $data;
    }
}