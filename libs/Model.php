<?php
/**
 * Created by PhpStorm.
 * User: erlan
 * Date: 15.03.2018
 * Time: 10:30
 */

class Model
{
    function isPropertiesIsNull($object) {
        foreach ($object as $key => $value) {
            if ($value == null || !isset($object[$key])) return true;
        }
        return false;
    }
}