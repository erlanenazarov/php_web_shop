<?php
/**
 * Created by PhpStorm.
 * User: erlan
 * Date: 15.03.2018
 * Time: 10:52
 */
define('PROJECT_PATH', __DIR__);

require_once ('parameters.php');
require_once ('libs/Database.php');

if (isset($argv[1])) {
    switch ($argv[1]) {
        case 'help':
            echo('`migrate` - Промигрировать структуру базы данных'.PHP_EOL);
            echo('`create_superuser` - Создать суперпользователя с почтой "admin@localhost" и паролем "123456789a"');
            break;
        default:
            echo('Введите `help` чтобы увидеть все доступные комманды'.PHP_EOL);
            break;
        case 'migrate':
            $db = new Database();
            echo $db->migrate();
            break;
        case 'create_superuser':
            $db = new Database();
            $user = array(
                'name' => 'admin',
                'email' => 'admin@localhost',
                'password' => md5('123456789a')
            );
            echo $db->query("INSERT INTO users(nickname, email, password, is_superuser) VALUES ('{$user['name']}', '{$user['email']}', '{$user['password']}', '1')");
            break;
    }
} else {
    echo('///////////////////////////////////'.PHP_EOL);
    echo('///Welcome to Dmitriy\'s console////'.PHP_EOL);
    echo('///////////////////////////////////'.PHP_EOL);

    echo(PHP_EOL);
    echo(PHP_EOL);

    echo('Введите `help` чтобы увидеть все доступные комманды'.PHP_EOL);
}

echo(PHP_EOL);